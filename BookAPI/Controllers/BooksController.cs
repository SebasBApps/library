﻿using BookAPI.Models;
using BookAPI.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookRepository bookRepository;
        public BooksController(IBookRepository bookRepository)
        {
            this.bookRepository = bookRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Book>> getBooks()
        {
            return await this.bookRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> getBooks(int id)
        {
            return await this.bookRepository.Get(id);
        }
        [HttpPost]
        public async Task<ActionResult<Book>> postBooks([FromBody] Book book)
        {
            var newBook = await this.bookRepository.Create(book);
            return CreatedAtAction(nameof(getBooks), new { id = newBook.idBook }, newBook);
        }
        [HttpPut]
        public async Task<ActionResult> putBooks(int id, [FromBody] Book book)
        {
            if (id != book.idBook)
                return BadRequest();

            await this.bookRepository.Update(book);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> deleteBook(int id)
        {
            var bookToDelete = await this.bookRepository.Get(id);
            if (bookToDelete == null)
                return NotFound();

            await this.bookRepository.Delete(id);
            return NoContent();
        }
    }
}
