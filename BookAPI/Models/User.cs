﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Models
{
    public class User
    {
        public int idUser { get; set; }
        public String name { get; set; }
        public DateTime birthDate { get; set; }

    }
}
