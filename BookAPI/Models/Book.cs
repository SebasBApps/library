﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Models
{
    public class Book
    {
        public int idBook { get; set; }
        public String title { get; set; }
        public String author { get; set; }
        public String description { get; set; }

    }
}
